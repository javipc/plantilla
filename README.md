
# Plantillas
Este programa PHP permite realizar documentos basados en plantillas.
Esto facilita trabajar con modelos y separa el código de la interfaz del resto del programa.
La nueva versión del manejador de base de datos JBDD [Más información](https://gitlab.com/javipc/) utiliza este sistema de plantillas. Por lo tanto no solo se reduce a documentos HTML sino que puede alcanzar cualquier documento en el que se necesite reemplazar datos.

Un ejemplo sencillo: SELECT {nombre_columna} FROM {nombre_tabla}


## Características:

### Inserta archivos

Insertar archivos en tan fácil como poner el nombre del archivo y listo.
Recomiendo dejar los archivos de tipo HTM para uso general, que sirvan para cualquier proyecto.
Por ejemplo publicidad, logo de la empresa, etc.
Y los archivos HTML para uso puntual de cada proyecto.


{{encabezado.htm}} {{pie.htm}} {{cualquier_archivo.txt}}


### Tipos de archivos

Puede insertar cualquier documento de texto, por ejemplo HTM, HTML, CSS, JS, SVG, TXT...


### Permite más de una variable con el mismo nombre

{{usuario}} {usuario}

{{usuario}} puede ser usado para decir "usuario"en diferentes idiomas.

{usuario} puede ser usado para mostrar el nombre del usuario actual.




### Personaliza los delimitadores a gusto

Si no te gusta el uso de llaves, puedes usar el delimitador que más te guste
{{{usuario}}} ((usuario)) 


### Arreglos

{agenda: {nombre} {telefono} }

En este ejemplo recorrerá el arreglo "agenda" y mostrará todos los nombres junto con el teléfono de cada contacto.


### Anidamiento

Es posible recorrer arreglos dentro de otros arreglos.

{agenda: {nombre} {telefonos: {telefono} } }



### Condiciones

Consulta por una variable, si existe y es verdadera entonces se mostrará lo que esté a continuación dentro de la condición.

{es_nuevo? {{bienvenida.htm}} }

En este caso, si es_nuevo es verdad insertará el archivo que dará la bienvenida.





## Más aplicaciones

Al no tener publicidad este proyecto se mantiene únicamente con donaciones.
Siguiendo este enlace tendrás más información y también más aplicaciones.
[Más información](https://gitlab.com/javipc/mas) 



